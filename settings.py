# Application settings
from pursuitlib.utils import get_env

APP_NAME = "http_monitor"

VERSION = "0.2.0"
DEV_STAGE = "BETA"
DISPLAY_VERSION = VERSION if DEV_STAGE is None else VERSION + " " + DEV_STAGE

TENANTS_FILE = get_env("TENANTS_FILE", "tenants.json")
DOWN_HOSTS_FILE = get_env("DOWN_HOSTS_FILE", "down.json")


# Email settings

SMTP_HOST = get_env("SMTP_HOST")
SMTP_USE_SSL = get_env("SMTP_USE_SSL", "False").lower() == "true"
SMTP_PORT = get_env("SMTP_PORT", 465 if SMTP_USE_SSL else 25)
SMTP_USER = get_env("SMTP_USER", "")
SMTP_PASSWORD = get_env("SMTP_PASSWORD", "")

EMAIL_FROM = get_env("EMAIL_FROM")
ADMIN_EMAIL = get_env("ADMIN_EMAIL")
