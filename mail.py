import smtplib
from email.message import EmailMessage
from typing import List, Tuple

from pursuitlib.utils import is_null_or_empty

from settings import SMTP_USE_SSL, SMTP_HOST, SMTP_PORT, SMTP_USER, SMTP_PASSWORD, EMAIL_FROM

MONITOR_LABEL = "HTTP Monitor"
FIRING_LABEL = "FIRING"
RESOLVED_LABEL = "RESOLVED"


def send_email_to_server(to: str, subject: str, content: str, server: smtplib.SMTP):
    message = EmailMessage()
    message['Subject'] = subject
    message.set_content(content, subtype='html')

    if not is_null_or_empty(SMTP_USER) and not is_null_or_empty(SMTP_PASSWORD):
        server.login(SMTP_USER, SMTP_PASSWORD)

    server.send_message(message, EMAIL_FROM, to)


def send_email(to: str, subject: str, content: str):
    if SMTP_USE_SSL:
        with smtplib.SMTP_SSL(SMTP_HOST, SMTP_PORT) as server:
            send_email_to_server(to, subject, content, server)
    else:
        with smtplib.SMTP(SMTP_HOST, SMTP_PORT) as server:
            send_email_to_server(to, subject, content, server)


def send_status_email(to: str, firing: List[Tuple[str, str]], resolved: List[str], firing_old: List[Tuple[str, str]],):
    hosts_down = len(firing) > 0 or len(firing_old) > 0

    subject = ""
    if hosts_down:
        if len(firing) + len(firing_old) > 1:
            subject = f"[{MONITOR_LABEL}/{FIRING_LABEL}] Multiple hosts are down"
        elif len(firing) > 0:
            subject = f"[{MONITOR_LABEL}/{FIRING_LABEL}] Host '{firing[0][0]}' is down"
        elif len(firing_old) > 0:
            subject = f"[{MONITOR_LABEL}/{FIRING_LABEL}] Some hosts are back up, but host '{firing_old[0][0]}' is still down"
    else:
        if len(resolved) > 1:
            subject = f"[{MONITOR_LABEL}/{RESOLVED_LABEL}] Multiple hosts are back up"
        else: subject = f"[{MONITOR_LABEL}/{RESOLVED_LABEL}] Host '{resolved[0]}' is back up"

    content = ""

    if len(firing) > 0:
        content += "<b>The following hosts just went down:</b><br>\n<br>\n"
        for host, message in firing:
            content += f"<b>{host}</b>: {message}<br>\n"
        content += "<br>\n<br>\n"

    if len(resolved) > 0:
        content += "<b>The following hosts just went back up:</b><br>\n<br>\n"
        for host in resolved:
            content += f"<b>{host}</b><br>\n"
        content += "<br>\n<br>\n"

    if len(firing_old) > 0:
        content += "<b>The following hosts are still down:</b><br>\n<br>\n"
        for host, message in firing_old:
            content += f"<b>{host}</b>: {message}<br>\n"
        content += "<br>\n<br>\n"

    send_email(to, subject, content)
