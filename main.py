#######################
# HTTP Monitor script #
#     By Pursuit      #
#######################

import json
import os
import sys
from http.client import OK
from typing import List, Tuple

import requests
from requests import HTTPError
from urllib3.exceptions import MaxRetryError

import mail
from settings import DOWN_HOSTS_FILE, APP_NAME, DISPLAY_VERSION, TENANTS_FILE, ADMIN_EMAIL


def try_connect(url: str):
    response = requests.get(url, allow_redirects=True)
    if response.status_code != OK:
        raise HTTPError(f"{response.status_code}")


def test_host(host: str):
    try_connect(f"http://{host}")
    try_connect(f"https://{host}")


def get_error_message(e: Exception) -> str:
    if isinstance(e, requests.exceptions.ConnectionError):
        e = e.args[0]
    if isinstance(e, MaxRetryError):
        e = e.reason
    return f"{type(e).__name__}: {e}"


def load_tenants():
    try:
        if not os.path.isfile(TENANTS_FILE):
            return []
        with open(TENANTS_FILE, "r") as tenants_file:
            tenants = tenants_file.read()
            return json.loads(tenants)
    except (Exception,) as e:
        print(f"Unable to parse tenants: {e}", file=sys.stderr)
        return []


def load_down_hosts() -> List[str]:
    try:
        if not os.path.isfile(DOWN_HOSTS_FILE):
            return []
        with open(DOWN_HOSTS_FILE, "r") as down_file:
            down = down_file.read()
            return json.loads(down)
    except (Exception,) as e:
        print(f"Unable to parse down hosts file: {e}", file=sys.stderr)
        return []


def save_down_hosts(down: List[str]):
    try:
        with open(DOWN_HOSTS_FILE, "w") as down_file:
            serialized = json.dumps(down)
            down_file.write(serialized)
    except (Exception,) as e:
        print(f"Unable to save down hosts file: {e}", file=sys.stderr)


def filter_hosts(tenant, hosts: List[str]) -> List[str]:
    if "hosts" not in tenant:
        return []

    tenant_hosts = []
    for host in hosts:
        if host in tenant["hosts"]:
            tenant_hosts.append(host)
    return tenant_hosts


def filter_errors(tenant, errors: List[Tuple[str, str]]) -> List[Tuple[str, str]]:
    if "hosts" not in tenant:
        return []

    tenant_errors = []
    for error in errors:
        if error[0] in tenant["hosts"]:
            tenant_errors.append(error)
    return tenant_errors


def main() -> int:
    print(f"Starting {APP_NAME} version {DISPLAY_VERSION}...")
    tenants = load_tenants()

    old_down_hosts = load_down_hosts()
    new_down_hosts = []

    firing = []
    resolved = []
    firing_old = []

    monitored_hosts = []
    for tenant in tenants:
        if "hosts" in tenant:
            monitored_hosts += tenant["hosts"]

    for host in monitored_hosts:
        try:
            test_host(host)
            print(f"Host {host} is up !")

            # If the host is up
            if host in old_down_hosts:
                resolved.append(host)
        except (Exception,) as e:
            # If the host is down
            print(f"Host {host} is down !")
            new_down_hosts.append(host)
            message = get_error_message(e)

            if host not in old_down_hosts:
                firing.append((host, message))
            else: firing_old.append((host, message))

    if len(firing) > 0 or len(resolved) > 0:  # If something has changed, send a new status email
        print("Sending status email to admin...")
        mail.send_status_email(ADMIN_EMAIL, firing, resolved, firing_old)

    for tenant in tenants:
        if "email" not in tenant:  # Do not email the tenant if no address was provided
            continue

        tenant_firing = filter_errors(tenant, firing)
        tenant_resolved = filter_hosts(tenant, resolved)
        tenant_firing_old = filter_errors(tenant, firing_old)

        if len(tenant_firing) > 0 or len(tenant_resolved) > 0:  # If something has changed for the tenant, send a new status email
            print(f"Sending status email to '{tenant}'...")
            mail.send_status_email(ADMIN_EMAIL, tenant_firing, tenant_resolved, tenant_firing_old)

    save_down_hosts(new_down_hosts)
    return 0


if __name__ == '__main__':
    sys.exit(main())
