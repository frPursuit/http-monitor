# HTTP Monitor

Simple HTTP webservice monitoring solution that sends emails when monitored hosts are down.

## Deploy the monitor in production

An Ansible playbook is provided in `/deploy` to help deploy the monitor in production.

By default, it is installed in `/opt/http_monitor`.

After running the playbook:

- Configure the applictaion by creating a `.env` file from `.env.example`
- Create tenants by creating a `tenants.json` file from `tenants.json.example`
- Enable the HTTP Monitor timer: `systemctl enable httpmon.timer`
- Start the HTTP Monitor timer: `systemctl start httpmon.timer`

## Tenant system

Several tenants can be defined in the `tenants.json`.
Each tenant is associated to a list of hosts.

When the state of a host changes, an email is sent to the corresponding tenant (if their `email` attribute is set), and another email is sent to the admin.

The admin's email can be set with the `ADMIN_EMAIL` environment variable.
